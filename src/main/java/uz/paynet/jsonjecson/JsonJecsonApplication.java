package uz.paynet.jsonjecson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonJecsonApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsonJecsonApplication.class, args);
    }

}
