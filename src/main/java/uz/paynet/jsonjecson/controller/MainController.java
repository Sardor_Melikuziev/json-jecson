package uz.paynet.jsonjecson.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.paynet.jsonjecson.action.Action;

import java.io.*;
import java.net.*;
import java.util.*;

@RestController
@RequestMapping("/json")
public class MainController {

    private static final String URL_STR ="";


    private Action action;

    @PostMapping("/test")
    public void test(@RequestParam("string") String string)throws IOException {
        List<String> list = new ArrayList<>();
        Map<String,Object> map=new HashMap<>();
        list.add("asfewevrevrgfe");
        map.put("asdsfa","wdcervrev");
        map.put("wefcreer",list);

        String json = new ObjectMapper().writeValueAsString(map);
        System.out.println(json);

        String query = "http://localhost:3001/api/test";
        String params = "{\"key\":1}";

        URL url = new URL(query);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setConnectTimeout(5000);
        conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestMethod("POST");

        OutputStream os = conn.getOutputStream();
        os.write(json.getBytes("UTF-8"));
        os.close();

        // read the response
        InputStream in = new BufferedInputStream(conn.getInputStream());
        //String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
        //JSONObject jsonObject = new JSONObject(result);


        in.close();
        conn.disconnect();

        //return jsonObject;

    }
}
