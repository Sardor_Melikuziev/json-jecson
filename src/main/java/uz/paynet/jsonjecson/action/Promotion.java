package uz.paynet.jsonjecson.action;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@Slf4j
@JsonTypeName("Promotion")
public class Promotion implements Action {

    @Override
    public String getName(String string) {
        return "Promotion";
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("This is the promotion class");
    }
}
