package uz.paynet.jsonjecson.action;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@Slf4j
@JsonTypeName("AutoPay")
public class AutoPay implements Action {

    @Override
    public String getName(String string) {
        return "AutoPay";
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("This is the auto pay class");
    }
}
