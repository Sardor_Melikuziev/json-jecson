package uz.paynet.jsonjecson.action;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.quartz.Job;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "@ttype")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AutoPay.class, name = "AutoPay"),
        @JsonSubTypes.Type(value = Monitoring.class, name = "Monitoring"),
        @JsonSubTypes.Type(value = Promotion.class, name = "Promotion")
})
@JsonIgnoreProperties(ignoreUnknown = true)
public interface Action extends Job {
    @JsonProperty("@ttype")
    String getName(String string);

}
